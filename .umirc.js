export default {
  disableServiceWorker: true,
  disableDynamicImport: true,
  exportStatic: {},
  pages: {
    "map": { document: "./map.ejs" }
  }
}
